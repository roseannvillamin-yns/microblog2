<?php

class Comment extends AppModel
{
    public $belongsTo = array(
        'user_id' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
        ),
        'post_id' => array(
            'className' => 'Post',
            'foreignKey' => 'post_id',
        )
    );

    public $validate = array(
        'comment' => array(
            'length' => array(
                'required' => false,
                'allowEmpty' => true,
                'rule' => array('maxLength', 140),
                'message' => 'Maximum length of 140 characters.'
            )
        )
    );
}
