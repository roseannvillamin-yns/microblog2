<?php

class Follower extends AppModel
{
    public $belongsTo = array(
        'user_id' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
        ),
        'follower_user_id' => array(
            'className' => 'User',
            'foreignKey' => 'follower_user_id',
        )
    );
}
