<?php

class Post extends AppModel
{
    public $belongsTo = array(
        'user_id' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );

    public $validate = array(
        'post' => array(
            'length' => array(
               'rule' => array('maxLength', 140),
               'message' => 'Maximum length of 140 characters.')
        ),
        'image' => array(
            'extension' => array(
                'rule' => array('extension', array('jpeg', 'jpg', 'png', 'gif', '')),
                'required' => false,
                'allowEmpty' => true,
                'message' => 'Image not valid.'
            ),
            /*'imageSize' => array(
                'rule' => array('fileSize', '<=', '2MB'),
                'message' => 'Image must be less than 2MB'
            )*/
        )
    );

    public function isOwnedBy($post, $user)
    {
        return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
    }
}
