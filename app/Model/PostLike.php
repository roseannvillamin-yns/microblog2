<?php

class PostLike extends AppModel
{
    public $belongsTo = array(
        'post_id' => array(
            'className' => 'Post',
            'foreignKey' => 'post_id',
        ),
        'user_id' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
        )
    );
}
