<?php

class User extends AppModel
{

    public $validate = array(
        'full_name' => array(
            'alphabet' => array(
                'rule' => '/^[a-zA-Z.\s]*$/',
                'message' => 'Only letters, white spaces and dot are allowed.'
            ),
            'whiteSpace' => array(
                'rule' => 'notBlank',
                'message' => 'White spaces only are not allowed.'
            ),
            'between' => array(
               'rule' => array('minLength', 3),
               'message' => 'Minimum length of 3 characters.')
        ),
        'username' => array(
            'alphaNumeric' => array(
                'rule' => '/^[a-zA-Z0-9_]*$/',
                'message' => 'Letters, numbers and underscore only.'
            ),
            'between' => array(
                'rule' => array('lengthBetween', 5, 20),
                'message' => 'Between 5 to 20 characters only.'
            ),
            'unique' => array(
                'rule' => array('isUnique'),
                'message' => 'This username is already in use.'
            ),
        ),
        'email' => array(
            'email_rule' => array(
                'rule' => 'email',
                'message' => 'Invalid email.'
            ),
            'unique' => array(
                'rule' => array('isUnique'),
                'message' => 'This email is already in use.'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Password field is required.'
            ),
            'length' => array(
                'rule' => array('minLength', 8),
                'message' => 'Password should have atleast 8 characters.'
            )
        ),
        'profile_pic' => array(
            'extension'=>array(
                'rule' => array('extension', array('jpeg', 'jpg', 'png', 'gif', '')),
                'required' => false,
                'allowEmpty' => true,
                'message' => 'Image not valid.'
            ),
            /*'imageSize' => array(
                'rule' => array('fileSize', '<=', '2MB'),
                'message' => 'Image must be less than 2MB'
            )*/
        ),
        'new_password' => array(
            'length' => array(
                'rule' => array('minLength', 8),
                'required' => false,
                'allowEmpty' => true,
                'message' => 'Password should have atleast 8 characters.'
            )
        ),
    );
}
