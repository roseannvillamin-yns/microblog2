<?php

App::uses('AppController', 'Controller');

class FollowersController extends AppController
{
    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash', 'Paginator');

    public function follow($id)
    {
        $following = $this->User->findById($id);
        if ($following != null) {
            $conditions = array(
                'Follower.user_id' => $following['User']['id'],
                'Follower.follower_user_id' => $this->Auth->user('id')
            );
            $follower = $this->Follower->find(
                'first',
                array(
                    'conditions' => $conditions
                )
            );

            if (!empty($follower)) {
                if ($follower['Follower']['deleted'] == 1) {
                    $follower['Follower']['deleted'] = 0;
                    $follower['Follower']['modified'] = date('Y-m-d H:i:s');
                    $this->Follower->save($follower);
                    $this->Flash->success(__('Followed'));
                } else {
                    $this->Flash->success(__('You are already follower of this user.'));
                }
            } else {
                $this->Follower->create();
                $this->request->data['Follower']['user_id'] = $following['User']['id'];
                $this->request->data['Follower']['follower_user_id'] = $this->Auth->user('id');
                if ($this->Follower->save($this->request->data)) {
                    $this->Flash->success(__('Followed'));
                } else {
                    $this->Flash->error(__('Cant follow.'));
                }
            }
        } else {
            $this->Flash->error(__('Data does not exist.'));
        }
        return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
    }

    public function unfollow($id)
    {
        $following = $this->User->findById($id);
        if ($following != null) {
            $conditions = array(
                'Follower.user_id' => $following['User']['id'],
                'Follower.follower_user_id' => $this->Auth->user('id')
            );
            $follower = $this->Follower->find(
                'first',
                array(
                    'conditions' => $conditions
                )
            );

            if (!empty($follower)) {
                if ($follower['Follower']['deleted'] == 0) {
                    $follower['Follower']['deleted'] = 1;
                    $follower['Follower']['modified'] = date('Y-m-d H:i:s');
                    $this->Follower->save($follower);
                    $this->Flash->success(__('Unfollowed'));
                } else {
                    $this->Flash->error(__('Failed to unfollow.'));
                }
            } else {
                $this->Flash->error(__('You are not following this user.'));
            }
        } else {
            $this->Flash->error(__('Data does not exist.'));
        }

        return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
    }

    public function viewFollowers()
    {
        $followers = array(
            'Follower.user_id' => $this->Auth->user('id')
        );
        $this->Paginator->settings = array(
            'conditions' => $followers,
            'order' => array('modified' => 'desc'),
            'limit' => 10
        );
        $this->set('userFollowers', $this->Paginator->paginate('Follower'));
        $this->set('title', 'Microblog 2 - Followers');
        return $this->userInfo();
    }

    public function viewFollowing()
    {
        $following = array(
        'Follower.follower_user_id' => $this->Auth->user('id')
        );
        $this->Paginator->settings = array(
            'conditions' => $following,
            'order' => array('modified' => 'desc'),
            'limit' => 10
        );
        $this->set('userFollowing', $this->Paginator->paginate('Follower'));
        $this->set('title', 'Microblog 2 - Following');
        return $this->userInfo();
    }
}
