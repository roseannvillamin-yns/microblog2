<?php

App::uses('AppController', 'Controller');

class RetweetsController extends AppController
{
    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash');

    public function retweet($id)
    {
        $post = $this->Post->findById($id);
        $conditions = array(
        'Retweet.post_id' => $post['Post']['id'],
        'Retweet.user_id' => $this->Auth->user('id')
            );
        $retweet = $this->Retweet->find(
            'first',
            array(
                'conditions' => $conditions
            )
        );
        if (!empty($retweet)) {
            if ($retweet['Retweet']['deleted'] == 1) {
                $retweet['Retweet']['deleted'] = 0;
                $retweet['Retweet']['modified'] = date('Y-m-d H:i:s');
                $this->Retweet->save($retweet);
                $this->Flash->success(__('Retweeted'));
            } else {
                $this->Flash->success(__('You already retweeted this post.'));
            }
        } else {
            $this->Retweet->create();
            $this->request->data['Retweet']['post_id'] = $post['Post']['id'];
            $this->request->data['Retweet']['user_id'] = $this->Auth->user('id');
            if ($this->Retweet->save($this->request->data)) {
                $this->Flash->success(__('Retweeted'));
            } else {
                $this->Flash->error(__('Failed to retweet.'));
            }
        }

        return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
    }

    public function delete($id)
    {
        $retweet = $this->Retweet->findById($id);
        if ($this->Auth->user('id') == $retweet['Retweet']['user_id']) {
            if ($retweet['Retweet']['deleted'] == 0) {
                $retweet['Retweet']['deleted'] = 1;
                $retweet['Retweet']['modified'] = date('Y-m-d H:i:s');
                $this->Retweet->save($retweet);
                $this->Flash->success(__('Retweet deleted'));
            } else {
                $this->Flash->error(__('Retweet could not be deleted.'));
            }
        }
        return $this->redirect(array(
            'controller' => 'posts',
            'action' => 'index'
        ));
    }

    public function isAuthorized($user)
    {
        if ($this->action === 'add') {
            return true;
        }

        if (in_array($this->action, array('edit', 'delete'))) {
            $retweetId = (int) $this->request->params['pass'][0];
            $retweetUserId = $this->Retweet->findById($retweetId);
            if ($this->Acl->check('users', 'controllers')) {
                if ($retweetUserId != null) {
                    if ($retweetUserId['Retweet']['user_id'] != $user['id']) {
                        $this->Flash->error(__('You are not authorized to that retweeted post.'));
                        return false;
                    }
                } else {
                    $this->Flash->error(__('Data does not exist.'));
                    return false;
                }
            }
        }

        return parent::isAuthorized($user);
    }
}
