<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class UsersController extends AppController
{
    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash', 'Paginator', 'Session');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(
            'index',
            'signup',
            'logout',
            'verification',
            'forgotPassword',
            'changePassword'
        );

        if (in_array($this->request->action, ['signup']) && !empty($this->Auth->user('id'))) {
            $this->Flash->error(__('You are logged in!'));
            return $this->redirect($this->Auth->redirectUrl());

        }
    }

    public function initDB()
    {
        $aro = $this->Acl->Aro;
        $groups = array(
            0 => array(
                'alias' => 'users'
            )
        );

        foreach ($groups as $data) {
            $aro->create();
            $aro->save($data);
        }
    }

    public function index()
    {
        return $this->redirect(array('action' => 'login'));
        $this->set('title', 'Microblog 2');
    }

    public function signup()
    {
        if ($this->request->is('post')) {
            $this->User->create();
            $code = md5($this->request->data['User']['username']);
            $this->request->data['User']['email_token'] = $code;
            $recipient = $this->request->data['User']['email'];
            $username = $this->request->data['User']['username'];

            $this->User->set($this->request->data);
            if ($this->User->validates()) {
                $passwordHasher = new BlowfishPasswordHasher();
                $this->request->data['User']['password'] = $passwordHasher->hash(
                    $this->request->data['User']['password']
                );
                if ($this->User->save($this->request->data)) {
                    $body = 'To complete the registration, kindly click the link for activation: ';
                    $link = 'http://dev7.ynsdev.pw/users/verification/' . $username . '/' . $code;
                    $message = $body . $link;
                    $email = new CakeEmail('gmail');
                    $email->from(array('cakemicroblog@gmail.com' => 'Microblog CakePHP 2'))
                        ->to($recipient)
                        ->subject('Account Activation')
                        ->send($message);
                        $this->Flash->success(__('The user has been saved. Kindly check your email.'));

                    return $this->redirect(array('action' => 'index'));
                }
            } else {
                $errors = $this->User->validationErrors;
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set('title', 'Microblog 2 - Signup Page');
        $this->layout = 'navbar_signup';
    }

    public function verification($username, $code)
    {
        $user = $this->User->findByUsername($username);
        if ($user['User']['verified'] == 0 && $user['User']['email_token'] === $code) {
            $user['User']['verified'] = 1;
            $aro = new Aro();
            $users = array(
                0 => array(
                    'alias' => $user['User']['username'],
                    'parent_id' => 1,
                    'model' => 'User'
                )
            );

            foreach ($users as $data) {
                $aro->create();
                $aro->save($data);
            }
            $this->User->save($user);
            $this->Flash->success(__('Registration Completed.'));
            $this->redirect(array('action' => 'login'));
        } else {
            $this->Flash->error(__('Activation Failed / Account already verified.'));
            $this->redirect(array('action' => 'index'));
        }
    }

    public function login()
    {
        if ($this->Session->read('Auth.User')) {
            $this->Flash->error(__('You are logged in!'));
            return $this->redirect($this->Auth->redirectUrl());
        }
        if ($this->request->is('post')) {
            $username = $this->request->data['User']['username'];
            $user = $this->User->findByUsername($username);
            if ($user != null) {
                if ($this->Auth->login()) {
                    if ($user['User']['verified'] == 1) {
                        $this->Flash->success(__('Successfully logged in!'));
                        return $this->redirect($this->Auth->redirectUrl());
                    } else {
                        $this->Session->destroy('Auth.User');
                    }
                }
            }
            $this->Flash->error(__(
                'Invalid username/password, or your account is not yet verified. Try again.'
            ));
        }
        $this->set('title', 'Microblog 2 - Login Page');
        $this->layout = 'navbar_login';
    }

    public function logout()
    {
        $this->Session->destroy('Auth.User');
        $this->Flash->success(__('You have successfully logged out!'));
        return $this->redirect($this->Auth->logout());
    }

    public function view($username = null)
    {
        if (!$username) {
            throw new NotFoundException(__('Invalid user'));
        }

        $user = $this->User->findByUsername($username);
        $this->set('user', $user);
        if (!$user) {
            throw new NotFoundException(__('Invalid User'));
        }

        $conditions = array(
            'Post.deleted' => 0,
            'Post.user_id' => $user['User']['id']
        );
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order' => array('created' => 'desc'),
            'contain' => 'post_id',
            'limit' => 10
        );
        $this->set('posts', $this->Paginator->paginate('Post'));

        $likeConditions = array(
            'PostLike.user_id' => $this->Auth->user('id'),
            'PostLike.deleted' => 0
        );
        $likes = $this->PostLike->find(
            'all',
            array(
                'conditions' => $likeConditions,
                'contain' => 'user_id', 'post_id',
                'order' => array('PostLike.created' => 'desc'),
            )
        );
        $this->set('likes', $likes);
        $this->set('title', "Microblog 2 - User's Page");
        return $this->userInfo();
    }

    public function edit($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid user'));
        }

        $user = $this->User->findById($id);
        $this->set('user', $user);
        if (!$id) {
            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->Auth->user('id') == $user['User']['id']) {
            if ($this->request->is(array('post', 'put'))) {
                $this->User->id = $id;
                $currentProfile = $user['User']['profile_pic'];
                $updatedProfile = $this->request->data['User']['profile_pic']['name'];
                $currentPassword = $user['User']['password'];
                $updatedPassword = $this->request->data['User']['new_password'];

                $this->User->set($this->request->data);
                if ($this->User->validates()) {
                    if (!empty($updatedPassword)) {
                        $passwordHasher = new BlowfishPasswordHasher();
                        $this->request->data['User']['password'] = $passwordHasher->hash(
                            $updatedPassword
                        );
                    } else {
                        $this->request->data['User']['password'] = $currentPassword;
                    }

                    if (!empty($updatedProfile)) {
                        $imagePath = WWW_ROOT . 'img/profiles/' . $updatedProfile;
                        move_uploaded_file($this->request->data['User']['profile_pic']['tmp_name'], $imagePath);
                        $this->request->data['User']['profile_pic'] = $updatedProfile;
                    } else {
                        $this->request->data['User']['profile_pic'] = $currentProfile;
                    }

                    if ($this->User->save($this->request->data)) {
                        $this->Session->write(
                            'Auth.User',
                            array_merge(
                                $this->Auth->user(),
                                $this->request->data['User']
                            )
                        );
                        $this->Flash->success(__('Your profile has been updated.'));
                        return $this->redirect(
                            array(
                                'action' => 'view',
                                $this->request->data['User']['username']
                            )
                        );
                    }
                } else {
                    $errors = $this->User->validationErrors;
                }
                $this->Flash->error(__('Unable to update your profile.'));
            }
        } else {
            $this->Flash->error(__('You are not able to edit this profile.'));
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        }

        if (!$this->request->data) {
            $this->request->data = $user;
        }
        $this->set('title', 'Microblog 2 - Account Settings');
        return $this->userInfo();
    }

    public function viewUsers()
    {
        $users = array(
            'User.verified' => 1
        );
        $this->Paginator->settings = array(
            'conditions' => $users,
            'order' => array('created' => 'desc'),
            'limit' => 10
        );
        $this->set('users', $this->Paginator->paginate('User'));
        $this->set('title', 'Microblog 2 - All Users');
        return $this->userInfo();
    }

    public function isAuthorized($user)
    {
        if ($this->action === 'add') {
            return true;
        }

        if (in_array($this->action, array('edit', 'delete'))) {
            $id = (int) $this->request->params['pass'][0];
            $userId = $this->User->findById($id);
            if ($this->Acl->check('users', 'controllers')) {
                if ($userId != null) {
                    if ($userId['User']['id'] != $user['id']) {
                        $this->Flash->error(__("You are not authorized to that user's account."));
                        return false;
                    }
                } else {
                    $this->Flash->error(__('Data does not exist.'));
                    return false;
                }
            }
        }

        return parent::isAuthorized($user);
    }

    public function forgotPassword()
    {
        if ($this->request->is(array('post', 'put'))) {
            $user = $this->User->findByEmail($this->request->data['User']['email']);
            if (!empty($user) && $user['User']['verified'] == 1) {
                $this->User->id = $user['User']['id'];
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $code = substr(str_shuffle($characters), 0, 32);
                $user['User']['verified'] = 0;
                $user['User']['email_token'] = $code;
                $recipient = $user['User']['email'];
                $username = $user['User']['username'];
                $user['User']['modified'] = date('Y-m-d H:i:s');

                if ($this->User->save($user)) {
                    $body = 'Kindly click the link to change password: ';
                    $link = 'http://dev7.ynsdev.pw/users/changePassword/' . $code;
                    $message = $body . $link;
                    $email = new CakeEmail('gmail');
                    $email->from(array('cakemicroblog@gmail.com' => 'Microblog CakePHP 2'))
                        ->to($recipient)
                        ->subject('Change Password')
                        ->send($message);
                        $this->Flash->success(__('Email Sent'));

                    return $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Flash->error(__(
                    'Invalid email or your account is not yet verified. Try again.'
                ));
            }
        }
        $this->layout = 'navbar_signup';
        $this->set('title', 'Microblog 2 - Forgot Password');
    }

    public function changePassword($code = null)
    {
        if (!$code) {
            throw new NotFoundException(__('Invalid code'));
        }

        $user = $this->User->findByEmailToken($code);
        if (!$user) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($user['User']['verified'] == 0 && $user['User']['email_token'] === $code) {
            if ($this->request->is(array('post', 'put'))) {
                $user['User']['verified'] = 1;
                $newPassword = $this->request->data['User']['password'];
                $user['User']['modified'] = date('Y-m-d H:i:s');
                $this->User->set($this->request->data);
                if ($this->User->validates(array('fieldList' => array('password')))) {
                    if (!empty($newPassword)) {
                        $passwordHasher = new BlowfishPasswordHasher();
                        $user['User']['password'] = $passwordHasher->hash(
                            $newPassword
                        );
                    }

                    if ($this->User->save($user)) {
                        $this->Flash->success(__('Successfully changed your password.'));
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Flash->error(__('Failed to change your password.'));
                        $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $errors = $this->User->validationErrors;
                }
            }
        } else {
            $this->Flash->error(__('Email token already used.'));
            $this->redirect(array('action' => 'index'));
        }
        $this->layout = 'navbar_signup';
        $this->set('title', 'Microblog 2 - Change Password');
    }
}
