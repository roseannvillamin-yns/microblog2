<?php

App::uses('AppController', 'Controller');

class CommentsController extends AppController
{
    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash', 'Paginator');

    public function add($id = null)
    {
        $post = $this->Post->findById($id);
        $this->set('post', $post);
        $conditions = array(
            'Comment.deleted' => 0,
            'Comment.post_id' => $post['Post']['id']
        );
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order' => array('created' => 'desc'),
            'limit' => 5
        );
        $this->set('postComments', $this->Paginator->paginate('Comment'));
        $commentCount = $this->Comment->find(
            'count',
            array(
                'conditions' => $conditions,
                'contain' => 'user_id', 'post_id'
            )
        );
        $this->set('commentCount', $commentCount);

        if ($this->request->is('post')) {
            $this->Comment->create();
            $this->Comment->set($this->request->data);
            if ($this->Comment->validates()) {
                $this->request->data['Comment']['post_id'] = $post['Post']['id'];
                $this->request->data['Comment']['user_id'] = $this->Auth->user('id');
                if ($this->Comment->save($this->request->data)) {
                    $this->Flash->success(__('Comment added.'));
                    return $this->redirect(array(
                        'controller' => 'comments',
                        'action' => 'add',
                        $post['Post']['id']
                    ));
                }
            } else {
                $errors = $this->Comment->validationErrors;
            }
            $this->Flash->error(__('Unable to add your comment.'));
        }
        $this->set('title', 'Microblog 2 - Post Comments');
        return $this->userInfo();
        
    }

    public function edit($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid comment'));
        }

        $comment = $this->Comment->findById($id);
        $this->set('comment', $comment);
        if (!$comment) {
            throw new NotFoundException(__('Invalid comment'));
        }

        if ($comment['Comment']['deleted'] == 0) {
            if ($this->request->is(array('post', 'put'))) {
                $this->Comment->set($this->request->data);
                if ($this->Comment->validates()) {
                    $this->Comment->id = $id;
                    if ($this->Comment->save($this->request->data)) {
                        $this->Flash->success(__('Your comment has been updated.'));
                        return $this->redirect(array(
                            'controller' => 'comments',
                            'action' => 'add',
                            $comment['Comment']['post_id']
                        ));
                    }
                } else {
                    $errors = $this->Comment->validationErrors;
                }
                $this->Flash->error(__('Unable to update your comment.'));
            }
        } else {
            $this->Flash->error(__('Data does not exist.'));
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        }
        
        if (!$this->request->data) {
            $this->request->data = $comment;
        }
        $this->set('title', 'Microblog 2 - Edit Comment');
        return $this->userInfo();
    }

    public function delete($id)
    {
        $comment = $this->Comment->findById($id);
        if ($this->Auth->user('id') == $comment['Comment']['user_id']) {
            if ($comment['Comment']['deleted'] == 0) {
                $comment['Comment']['deleted'] = 1;
                $this->Comment->save($comment);
                $this->Flash->success(__('Comment deleted'));
            } else {
                $this->Flash->error(__('Comment could not be deleted.'));
            }
        }
        return $this->redirect(array(
            'controller' => 'comments',
            'action' => 'add',
            $comment['Comment']['post_id']
        ));
    }

    public function isAuthorized($user)
    {
        if ($this->action === 'add') {
            return true;
        }

        if (in_array($this->action, array('edit', 'delete'))) {
            $commentId = (int) $this->request->params['pass'][0];
            $commentUserId = $this->Comment->findById($commentId);
            if ($this->Acl->check('users', 'controllers')) {
                if ($commentUserId != null) {
                    if ($commentUserId['Comment']['user_id'] != $user['id']) {
                        $this->Flash->error(__('You are not authorized to that comment.'));
                        return false;
                    }
                } else {
                    $this->Flash->error(__('Data does not exist.'));
                    return false;
                }
            }
        }

        return parent::isAuthorized($user);
    }
}
