<?php

App::uses('AppController', 'Controller');

class PostLikesController extends AppController
{
    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash', 'Paginator');

    public function like($id)
    {
        $post = $this->Post->findById($id);
        if ($post != null) {
            $conditions = array(
                'PostLike.user_id' => $this->Auth->user('id'),
                'PostLike.post_id' => $post['Post']['id']
            );

            $likePost = $this->PostLike->find(
                'first',
                array(
                    'conditions' => $conditions
                )
            );

            if (!empty($likePost)) {
                if ($likePost['PostLike']['deleted'] == 1) {
                    $likePost['PostLike']['deleted'] = 0;
                    $likePost['PostLike']['modified'] = date('Y-m-d H:i:s');
                    $this->PostLike->save($likePost);
                    $this->Flash->success(__('Liked'));
                } else {
                    $this->Flash->success(__('You already liked this post.'));
                }
            } else {
                $this->PostLike->create();
                $this->request->data['PostLike']['post_id'] = $post['Post']['id'];
                $this->request->data['PostLike']['user_id'] = $this->Auth->user('id');
                if ($this->PostLike->save($this->request->data)) {
                    $this->Flash->success(__('Liked'));
                } else {
                    $this->Flash->error(__('Error'));
                }
            }
        } else {
            $this->Flash->error(__('Data does not exist.'));
        }
        return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
    }

    public function dislike($id)
    {
        $post = $this->Post->findById($id);
        if ($post != null) {
            $conditions = array(
                'PostLike.post_id' => $post['Post']['id'],
                'PostLike.user_id' => $this->Auth->user('id')
            );

            $likePost = $this->PostLike->find(
                'first',
                array(
                    'conditions' => $conditions
                )
            );
            if (!empty($likePost)) {
                if ($likePost['PostLike']['deleted'] == 0) {
                    $likePost['PostLike']['deleted'] = 1;
                    $likePost['PostLike']['modified'] = date('Y-m-d H:i:s');
                    $this->PostLike->save($likePost);
                    $this->Flash->success(__('Disliked'));
                } else {
                    $this->Flash->error(__('Like first the post.'));
                }
            } else {
                $this->Flash->error(__('Like first the post.'));
            }
        } else {
            $this->Flash->error(__('Data does not exist.'));
        }
        return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
    }
}
