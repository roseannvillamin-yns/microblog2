<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @package   app.Controller
 * @since     CakePHP(tm) v 0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package app.Controller
 * @link    https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public $uses = array('User', 'Follower', 'Post', 'Retweet', 'PostLike', 'Comment');

    public $components = array(
        //'DebugKit.Toolbar',
        'Flash',
        'Acl',
        'Session',
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'posts',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'index'
            ),
            'unauthorizedRedirect' => array(
                'controller' => 'posts',
                'action' => 'index'
            ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            ),
            'authorize' => array('Controller')
        )
    );

    public function beforeFilter()
    {
        $this->Session->delete('Auth.redirect');
        $this->set('loggedIn', $this->Auth->loggedIn());
        $this->set('username', $this->Session->read('Auth.User.username'));
        $this->set('id', $this->Session->read('Auth.User.id'));
        $this->set('profile', $this->Session->read('Auth.User.profile_pic'));
        $this->set('name', $this->Session->read('Auth.User.full_name'));
        $this->set('email', $this->Session->read('Auth.User.email'));
        $this->Auth->flash['params']['class'] = 'alert alert-warning';
    }

    public function isAuthorized($user)
    {
        if (isset($user)) {
            return true;
        }
        return false;
    }

    public function userInfo()
    {
        $retweetConditions = array(
            'OR' => array(
                'Retweet.user_id' => AuthComponent::user('id'),
                array(
                    'Follower.follower_user_id' => AuthComponent::user('id'),
                    'Follower.deleted' => 0
                )
            ),
            'Retweet.deleted' => 0
        );
        $retweet = $this->Retweet->find(
            'all',
            array(
                'conditions' => $retweetConditions,
                'joins' => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'inner',
                        'conditions' => array(
                            'User.id = Retweet.user_id'
                        )
                    ),
                    array(
                        'table' => 'posts',
                        'alias' => 'Post',
                        'type' => 'left',
                        'conditions' => array(
                            'Post.id = Retweet.post_id'
                        )
                    ),
                    array(
                        'table' => 'followers',
                        'alias' => 'Follower',
                        'type' => 'left',
                        'conditions' => array(
                            'Follower.user_id = Retweet.user_id'
                        )
                    )
                ),
                'group' => array('Retweet.id'),
                'order' => array('Retweet.modified' => 'desc'),
                'contain' => 'post_id'
            )
        );
        $this->set('retweets', $retweet);

        $following = array(
            'Follower.follower_user_id' => $this->Auth->user('id'),
            'Follower.deleted' => 0
        );
        $this->set('following', $this->Follower->find(
            'all',
            array(
                'conditions' => $following,
                'order' => array('Follower.modified' => 'desc'),
                'limit' => 5,
                'contain' => 'user_id', 'follower_user_id'
            )
        ));

        $followers = array(
            'Follower.user_id' => $this->Auth->user('id'),
            'Follower.deleted' => 0
        );
        $this->set('followers', $this->Follower->find(
            'all',
            array(
                'conditions' => $followers,
                'order' => array('Follower.modified' => 'desc'),
                'limit' => 5,
                'contain' => 'user_id', 'follower_user_id'
            )
        ));

        $followerCount = $this->Follower->find(
            'count',
            array(
                'conditions' => $followers,
                'contain' => 'user_id', 'follower_user_id'
            )
        );
        $this->set('followerCount', $followerCount);

        $followingCount = $this->Follower->find(
            'count',
            array(
                'conditions' => $following,
                'order' => array('Follower.modified' => 'desc'),
                'contain' => 'user_id', 'follower_user_id'
            )
        );
        $this->set('followingCount', $followingCount);

        $likeConditions = array(
            'PostLike.user_id' => $this->Auth->user('id'),
            'PostLike.deleted' => 0
        );
        $likeCount = $this->PostLike->find(
            'count',
            array(
                'conditions' => $likeConditions,
                'contain' => 'user_id', 'post_id'
            )
        );
        $this->set('likeCount', $likeCount);
    }
}
