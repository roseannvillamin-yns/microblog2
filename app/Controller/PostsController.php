<?php

App::uses('AppController', 'Controller');

class PostsController extends AppController
{
    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash', 'Paginator');

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function index()
    {
        $allPost = array(
            'OR' => array(
                'Post.user_id' => AuthComponent::user('id'),
                array(
                    'Follower.follower_user_id' => AuthComponent::user('id'),
                    'Follower.deleted' => 0
                )
            ),
            'Post.deleted' => 0
        );
        $this->Paginator->settings = array(
            'conditions' => $allPost,
            'joins' => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'inner',
                    'conditions' => array(
                        'User.id = Post.user_id',
                    )
                ),
                array(
                    'table' => 'followers',
                    'alias' => 'Follower',
                    'type' => 'left',
                    'conditions' => array(
                        'Follower.user_id = Post.user_id',
                    )
                )
            ),
            'group' => array('Post.id'),
            'order' => array('Post.created' => 'desc'),
            'contain' => 'post_id',
            'limit' => 10
        );
        $this->set('posts', $this->Paginator->paginate('Post'));

        $likeConditions = array(
            'PostLike.user_id' => $this->Auth->user('id'),
            'PostLike.deleted' => 0
        );
        $likes = $this->PostLike->find(
            'all',
            array(
                'conditions' => $likeConditions,
                'contain' => 'user_id', 'post_id',
                'order' => array('PostLike.created' => 'desc'),
            )
        );
        $this->set('likes', $likes);

        $this->set('title', 'Microblog 2 - Home Page');
        return $this->userInfo();
    }

    public function add()
    {
        if ($this->request->is('post')) {
            $this->Post->create();
            $this->Post->set($this->request->data);
            if ($this->Post->validates()) {
                $this->request->data['Post']['user_id'] = $this->Auth->user('id');
                $image = $this->request->data['Post']['image']['name'];
                if (isset($image)) {
                    $imagePath = WWW_ROOT . 'img/postImages/' . $image;
                    move_uploaded_file($this->request->data['Post']['image']['tmp_name'], $imagePath);
                    $this->request->data['Post']['image'] = $image;
                }

                if ($this->Post->save($this->request->data)) {
                    $this->Flash->success(__('Your post has been added.'));
                    return $this->redirect(array('action' => 'index'));
                }
            } else {
                $errors = $this->Post->validationErrors;
            }
            $this->Flash->error(__('Unable to add your post.'));
        }
        $this->set('title', 'Microblog 2 - Add Post');
        return $this->userInfo();
    }

    public function edit(int $id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);
        $this->set('post', $post);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($post['Post']['deleted'] == 0) {
            if ($this->request->is(array('post', 'put'))) {
                $this->Post->id = $id;
                $currentImage = $post['Post']['image'];
                $updatedImage = $this->request->data['Post']['image']['name'];
                $this->Post->set($this->request->data);
                if ($this->Post->validates()) {
                    if (!empty($updatedImage)) {
                        $imagePath = WWW_ROOT . 'img/postImages/' . $updatedImage;
                        move_uploaded_file($this->request->data['Post']['image']['tmp_name'], $imagePath);
                        $this->request->data['Post']['image'] = $updatedImage;
                    } else {
                        $this->request->data['Post']['image'] = $currentImage;
                    }

                    if ($this->Post->save($this->request->data)) {
                        $this->Flash->success(__('Your post has been updated.'));
                        return $this->redirect(array('action' => 'index'));
                    }
                } else {
                    $errors = $this->Post->validationErrors;
                }
                $this->Flash->error(__('Unable to update your post.'));
            }
        } else {
            $this->Flash->error(__('Data does not exist.'));
            return $this->redirect(array('action' => 'index'));
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }
        $this->set('title', 'Microblog 2 - Edit Post');
        return $this->userInfo();
    }

    public function delete(int $id)
    {
        $post = $this->Post->findById($id);
        if ($this->Auth->user('id') == $post['Post']['user_id']) {
            if ($post['Post']['deleted'] == 0) {
                $post['Post']['deleted'] = 1;
                $post['Post']['deleted_date'] = date('Y-m-d H:i:s');
                $this->Post->save($post);
                $this->Flash->success(__('The post has been deleted.'));
            } else {
                $this->Flash->error(__('The post could not be deleted.'));
            }
        } else {
            $this->Flash->error(__('You are not able to delete this post.'));
        }

        return $this->redirect(array('action' => 'index'));
    }

    public function isAuthorized($user)
    {
        if ($this->action === 'add') {
            return true;
        }

        if (in_array($this->action, array('edit', 'delete'))) {
            $postId = (int) $this->request->params['pass'][0];
            $postUserId = $this->Post->findById($postId);
            if ($this->Acl->check('users', 'controllers')) {
                if ($postUserId != null) {
                    if ($postUserId['Post']['user_id'] != $user['id']) {
                        $this->Flash->error(__('You are not authorized to that post.'));
                        return false;
                    }
                } else {
                    $this->Flash->error(__('Data does not exist.'));
                    return false;
                }
            }
        }

        return parent::isAuthorized($user);
    }

    public function search()
    {
        if ($this->request->is('post')) {
            if (!empty($this->request->data['Post']['keyword'])) {
                $keyword = $this->request->data['Post']['keyword'];
                $conditions = array(
                    'OR' => array(
                        'user_id.full_name LIKE' => '%'.$keyword.'%',
                        'user_id.username LIKE' => '%'.$keyword.'%',
                        'Post.post LIKE' => '%'.$keyword.'%'
                    ),
                    'user_id.verified' => 1,
                    'Post.deleted' => 0
                );

                $this->Session->write('searchConditions', $conditions);
                $this->Session->write('keyword', $keyword);
            } else {
                $this->Flash->error(__('Please input any keyword.'));
                return $this->redirect(array('action' => 'index'));
            }
        }

        if ($this->Session->check('searchConditions')) {
            $conditions = $this->Session->read('searchConditions');
        } else {
            $conditions = null;
        }

        $this->paginate = array(
            'conditions' => $conditions,
            'order' => array('created' => 'desc'),
            'limit' => 5
        );
        $this->set('results', $this->Paginator->paginate('Post'));

        $likeConditions = array(
            'PostLike.user_id' => $this->Auth->user('id'),
            'PostLike.deleted' => 0
        );
        $likes = $this->PostLike->find(
            'all',
            array(
                'conditions' => $likeConditions,
                'contain' => 'user_id', 'post_id',
                'order' => array('PostLike.created' => 'desc'),
            )
        );
        $this->set('likes', $likes);
        $this->set('title', 'Microblog 2 - Search Results');
        return $this->userInfo();
    }

    public function view($id = null)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);
        $this->set('post', $post);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        $conditions = array(
            'Comment.deleted' => 0,
            'Comment.post_id' => $post['Post']['id']
        );
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'order' => array('created' => 'desc'),
            'limit' => 10
        );
        $this->set('postComments', $this->Paginator->paginate('Comment'));
        $commentCount = $this->Comment->find(
            'count',
            array(
                'conditions' => $conditions,
                'contain' => 'user_id', 'post_id'
            )
        );
        $this->set('commentCount', $commentCount);
        $this->set('title', 'Microblog 2 - Post Comments');
        return $this->userInfo();
    }
}
