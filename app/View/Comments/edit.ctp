<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
            	<div class='card2'>
            		<h3 align='center'>Blog Post</h3>
            		<hr>
            		
            		<?= $this->Form->create('Comment') ?>
                    <?= $this->Form->input('comment',
                        array(
                            'class' => 'form-control',
                        	'div' => array(
                        		'class' => $this->Form->isFieldError(
                                    'Comment.comment') ? 'alert alert-warning' : 'form-label-group'
                        	)
                        )
                    ) ?>
                    <br>
                    <?= $this->Form->button('Update Comment', array('class' => 'btn btn-primary')) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>
</body>
</html>