<html>
<head>
<title></title>
<?= $this->Html->css('style.css') ?>
</head>
<body class='text-center'>
<section id='login' class='container'>
    <div class='row'>
        <div class='col-md-6'>
            <div class='card'>
                <h1 class='h3 mb-3 font-weight-normal'>Create Account</h1>
                <?= $this->Html->image('user.png',
                    array('class' => 'avatar')
                ) ?>
                <br>
                <?= $this->Flash->render('auth') ?>
                <?= $this->Form->create('User',
                    array('class' => 'form-signin')
                ) ?>
                <?= $this->Form->input('full_name',
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Full name',
                        'div' => array(
                            'class' => $this->Form->isFieldError(
                                'User.full_name') ? 'alert alert-warning' : 'form-label-group'
                        )
                    )
                ) ?>
                <?= $this->Form->input('username',
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Username',
                        'div' => array(
                            'class' => $this->Form->isFieldError(
                                'User.username') ? 'alert alert-warning' : 'form-label-group'
                        )
                    )
                ) ?>
                <?= $this->Form->input('email',
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Email',
                        'div' => array(
                            'class' => $this->Form->isFieldError(
                                'User.email') ? 'alert alert-warning' : 'form-label-group'
                        )
                    )
                ) ?>
                <?= $this->Form->input('password',
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Password',
                        'div' => array(
                            'class' => $this->Form->isFieldError(
                                'User.password') ? 'alert alert-warning' : 'form-label-group'
                        )
                    )
                ) ?>
                <?= $this->Form->input('Show Password',
                    array(
                        'type' => 'checkbox',
                        'onclick' => 'myFunction()'
                    )
                ) ?>
                <br>
                <?= $this->Form->button('SIGN UP',
                    array('class' => 'btn btn-lg btn-primary btn-block')
                ) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>
</body>
</html>