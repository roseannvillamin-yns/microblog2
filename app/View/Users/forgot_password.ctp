<html>
<head>
<title></title>
<?= $this->Html->css('style.css') ?>
</head>
<body class='text-center'>
<section id='login' class='container'>
<div class='row'>
    <div class='col-md-6'>
        <div class='card'>
        <h1 class='h3 mb-3 font-weight-normal'>Forgot Password</h1>
        <?= $this->Html->image('user.png',
            array('class' => 'avatar')
        ) ?>
        <br>
        <?= $this->Form->create('User') ?>
        <?= $this->Form->input('email',
            array('class' => 'form-control',
                'placeholder' => 'Input your registered email',
                'div' => array(
                    'class' => $this->Form->isFieldError(
                        'User.email') ? 'alert alert-warning' : 'form-label-group'
                )
            )
        ) ?>
        <br>
        <?= $this->Form->button('Send Link',
            array('class' => 'btn btn-primary btn-sm')
        ) ?>
        <?= $this->Form->end() ?>
        </div>
    </div>
</div>
</section>
</body>
</html>