<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
            	<div class='card2'>
            		<h3 align='center'>User's Account</h3>
            		<hr>
            		<?= $this->Flash->render('auth') ?>
            		<?= $this->Form->create('User',
                        array(
                            'type' => 'file',
                            'class' => 'form-signin'
                        )
                    ) ?>
                    <?= $this->Form->input('full_name',
                        array(
                            'class' => 'form-control',
                            'placeholder' => 'Full name',
                            'div' => array(
                                'class' => $this->Form->isFieldError(
                                    'User.full_name') ? 'alert alert-warning' : 'form-label-group'
                            )
                        )
                    ) ?>
                    <?= $this->Form->input('username',
                        array(
                            'class' => 'form-control',
                            'placeholder' => 'Username',
                            'div' => array(
                                'class' => $this->Form->isFieldError(
                                    'User.username') ? 'alert alert-warning' : 'form-label-group'
                            )
                        )
                    ) ?>
                    <?= $this->Form->input('email',
                        array(
                            'class' => 'form-control',
                            'placeholder' => 'Email',
                            'div' => array(
                                'class' => $this->Form->isFieldError(
                                    'User.email') ? 'alert alert-warning' : 'form-label-group'
                            )
                        )
                    ) ?>
                    <?= $this->Form->input('new_password',
                        array(
                            'type' => 'password',
                            'id' => 'UserPassword',
                            'class' => 'form-control',
                            'placeholder' => 'New Password',
                            'div' => array(
                                'class' => $this->Form->isFieldError(
                                    'User.new_password') ? 'alert alert-warning' : 'form-label-group'
                            )
                        )
                    ) ?>
                    <?= $this->Form->input('Show Password',
                        array(
                            'type' => 'checkbox',
                            'onclick' => 'myFunction()'
                        )
                    ) ?>
                    <?= $this->Form->input('profile_pic',
                        array(
                            'type' => 'file',
                            'class' => 'form-control',
                            'div' => array(
                                'class' => $this->Form->isFieldError(
                                    'User.profile_pic') ? 'alert alert-warning' : 'form-label-group'
                            )
                        )
                    ) ?>
                    <br>
                    <?= $this->Form->button('UPDATE',
            			 array('class' => 'btn btn-primary')
            		) ?>
                    <?= $this->Form->end() ?>
            	</div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>



