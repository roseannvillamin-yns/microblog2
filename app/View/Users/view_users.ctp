<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
                <div class='card2'>
                    <h3 align='center'>All Users</h3>
                    <hr>
                    <?php foreach ($users as $user): ?>
                        <div class='card22'>
                            <?php if ($user['User']['profile_pic'] != null): ?>
                                <?= $this->Html->image('profiles/'. $user['User']['profile_pic'],
                                    array(
                                        'class' => 'img-circle'
                                    )
                                ) ?>
                            <?php else: ?>
                                <?= $this->Html->image('profiles/user.png',
                                    array(
                                        'class' => 'img-circle'
                                    )
                                ) ?>
                            <?php endif; ?>
                            <?= $user['User']['full_name'] ?>
                            <br>
                            <?= $this->Html->link($user['User']['username'],
                                array(
                                    'controller' => 'users',
                                    'action' => 'view',
                                    $user['User']['username']
                                ),
                                array('style' => 'text-decoration: none')
                            ) ?>
                            <?php if ($id != $user['User']['id']): ?>
                            <?= $this->Html->link('Follow', 
                                array(
                                    'controller' => 'followers',
                                    'action' => 'follow',
                                    $user['User']['id']
                                ),
                                array('style' => 'text-decoration: none')
                            ) ?>
                            <?= $this->Html->link('Unfollow',
                                array(
                                    'controller' => 'followers',
                                    'action' => 'unfollow',
                                    $user['User']['id']
                                ),
                                array('style' => 'text-decoration: none')
                            ) ?>
                            <?php endif;?>
                        </div>
                    <?php endforeach; ?>
                    <?= $this->Paginator->numbers(array('separator' => '')) ?>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>