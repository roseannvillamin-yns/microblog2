<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
            	<div class='card2'>
            		<h3 align='center'>USER'S PAGE</h3>
            		<hr>
            		<div class='text' align='center'>
            			<div class='chip'>
                            <?php if ($user['User']['profile_pic'] != null): ?>
                                <?= $this->Html->image('profiles/'. $user['User']['profile_pic'],
                                    array('class' => 'profile')
                                ) ?>
                            <?php else: ?>
                                <?= $this->Html->image('profiles/user.png',
                                    array('class' => 'profile')
                                ) ?>
                            <?php endif; ?>
                            <h2><?= h($user['User']['full_name']) ?></h2>
                            <p><?= h($user['User']['username']) ?></p>
            		    </div>
            	    </div>

            	    <?php foreach ($posts as $post): ?>
                        <?php if ($post['user_id']['profile_pic'] != null): ?>
                            <?= $this->Html->image('profiles/'. $post['user_id']['profile_pic'],
                                array(
                                    'class' => 'img-circle'
                                )
                            ) ?>
                        <?php else: ?>
                            <?= $this->Html->image('profiles/user.png',
                                array(
                                    'class' => 'img-circle'
                                )
                            ) ?>
                        <?php endif; ?>
                        <?= $this->Html->link($post['user_id']['username'],
                            array(
                                'controller' => 'users',
                                'action' => 'view',
                                $post['user_id']['username']
                            ),
                            array('style' => 'text-decoration: none')
                        ) ?>
            	    	<br>
            	    	<?= nl2br(h($post['Post']['post'])) ?>
            	    	<br>
            	    	<?php if ($post['Post']['image'] != null): ?>
                            <?php if (file_exists(WWW_ROOT . 'img/postImages/' . $post['Post']['image'])): ?>
                                <?= $this->Html->image('postImages/'. $post['Post']['image'],
                                    array('class' => 'img-post')
                                ) ?>
                            <?php else: ?>
                                <?= $this->Html->image('postImages/unavailable-image.jpg',
                                    array('class' => 'img-post')
                                ) ?>
                            <?php endif; ?>
                        <?php endif; ?>
            	    	<br>
            	    	<small><?= h($post['Post']['created']) ?></small>
            	    	<br>
                        <center>
                            <?= $this->Html->link('Comments',
                                array(
                                    'controller' => 'posts',
                                    'action' => 'view',
                                    $post['Post']['id']
                                ),
                                array('class' => 'btn btn-outline-secondary')
                            ) ?>
                            <?= ' ' . $this->Html->link('Retweet',
                                array(
                                    'controller' => 'retweets',
                                    'action' => 'retweet',
                                    $post['Post']['id']
                                ),
                                array('class' => 'btn btn-outline-info')
                            ) ?>
                            <?php foreach ($likes as $like): ?>
                                <?php if ($like['PostLike']['post_id'] == $post['Post']['id'] && $like['PostLike']['user_id'] == $id): ?>
                                    <?= ' ' . $this->Html->image('like.png',
                                        array(
                                            'height' => '20px',
                                            'width' => '20px'
                                        )
                                    ) ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?= ' ' . $this->Html->link('Like',
                                array(
                                    'controller' => 'postLikes',
                                    'action' => 'like',
                                    $post['Post']['id']
                                ),
                                array('class' => 'btn btn-outline-primary')
                            ) ?>
                            <?= ' ' . $this->Html->link('Dislike',
                                array(
                                    'controller' => 'postLikes',
                                    'action' => 'dislike',
                                    $post['Post']['id']
                                ),
                                array('class' => 'btn btn-outline-dark')
                            ) ?>
                            <?php if ($id == $post['Post']['user_id']): ?>
                                <?= ' ' . $this->Html->image('edit.png',
                                    array(
                                        'height' => '20px',
                                        'width' => '20px',
                                        'style' => 'margin-left: 210px',
                                        'url' => array(
                                            'controller' => 'posts',
                                            'action' => 'edit',
                                            $post['Post']['id']
                                        )
                                    )
                                ) ?>
                                <?= ' ' . $this->Html->link($this->Html->image('delete.png',
                                    array(
                                        'height' => '20px',
                                        'width' => '20px'
                                    )
                                ), array(
                                    'controller' => 'posts',
                                    'action' => 'delete',
                                    $post['Post']['id']
                                ),
                                array(
                                    'escape' => false,
                                    'confirm' => 'Are you sure you want to delete?'
                                )) ?>
                            <?php endif; ?>
                        </center>
                        <hr><br>
            	    <?php endforeach; ?>
            	    <?= $this->Paginator->numbers(array('separator' => '')) ?>
            	</div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>