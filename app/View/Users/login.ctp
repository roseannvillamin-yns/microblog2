<html>
<head>
<title></title>
<?= $this->Html->css('style.css') ?>
</head>
<body class='text-center'>
<section id='login' class='container'>
    <div class='row'>
        <div class='col-md-6'>
            <div class='card'>
                <h1 class='h3 mb-3 font-weight-normal'>Login Page</h1>
                <?= $this->Html->image('user.png',
                    array('class' => 'avatar')
                ) ?>
                <br>
                <?= $this->Flash->render('auth') ?>
                <?= $this->Form->create('User',
                    array('class' => 'form-signin')
                ) ?>
                <?= $this->Form->input('username',
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Username',
                        'div' => array(
                            'class' => 'form-label-group'
                        )
                    )
                ) ?>
                <?= $this->Form->input('password',
                    array('class' => 'form-control',
                        'placeholder' => 'Password',
                        'div' => array(
                            'class' => 'form-label-group'
                        )
                    )
                ) ?>
                <?= $this->Form->input('Show Password',
                    array(
                        'type' => 'checkbox',
                        'onclick' => 'myFunction()'
                    )
                ) ?>
                <br>
                <?= $this->Form->button('LOGIN',
                        array('class' => 'btn btn-lg btn-primary btn-block')
                ) ?>
                <?= $this->Form->end() ?>
                <br>
                <?= $this->Html->link('Forgot Password',
                    array(
                        'controller' => 'users',
                        'action' => 'forgotPassword'
                    ),
                    array('style' => 'text-decoration: none')
                ) ?>
            </div>
        </div>
    </div>
</section>
</body>
</html>