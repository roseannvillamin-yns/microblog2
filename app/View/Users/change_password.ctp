<html>
<head>
<title></title>
<?= $this->Html->css('style.css') ?>
</head>
<body class='text-center'>
<section id='login' class='container'>
<div class='row'>
    <div class='col-md-6'>
        <div class='card'>
        <h1 class="h3 mb-3 font-weight-normal">Change Password</h1>
        <?= $this->Html->image('user.png', array('class' => 'avatar')) ?>
        <br>
        <?= $this->Form->create('User') ?>
        <?= $this->Form->input('password',
            array('class' => 'form-control',
                'placeholder' => 'New Password',
                'div' => array(
                    'class' => $this->Form->isFieldError(
                        'User.password') ? 'alert alert-warning' : 'form-label-group'
                )
            )
        ) ?>
        <?= $this->Form->input('Show Password',
            array(
                'type' => 'checkbox',
                'onclick' => 'myFunction()'
            )
        ) ?>
        <br>
        <?= $this->Form->button('Save Password',
            array('class' => 'btn btn-primary btn-sm')
        ) ?>
        <?= $this->Form->end() ?>
        </div>
    </div>
</div>
</section>
</body>
</html>