<?= $this->Html->script('bootstrap.min4.js') ?>
<?= $this->Html->css('style.css') ?>
<nav class="navbar navbar-dark fixed-top bg-primary">
	<div class="container">
		<a class="navbar-brand" href="#">MICROBLOG 2</a>
		<div class="btn-group">
			<?= $this->Html->link('HOME',
			    array(
			    	'controller' => 'posts',
			    	'action' => 'index',
			    ),
			    array(
			    	'class' => 'nav-link',
			    	'style' => 'color: white'
			    )
			) ?>
			<div class="dropdown">
				<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
					<?php if ($loggedIn): ?>
						<?= $username ?>
					<?php endif; ?>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<li>
						<?= $this->Html->link('Logout',
							array(
								'controller' => 'users',
								'action' => 'logout'
							),
							array(
								'class' => 'dropdown-item'
							)
						) ?>
					</li>
				</ul>
			</div>
		</div>
	</div>
</nav>