<?= $this->Html->css('home.css') ?>
<div class='col-md-3'>
    <div class='card3'>
        <?php 
        if ($this->Session->read('keyword') != ''):
            $keyword = $this->Session->read('keyword');
        else:
            $keyword = '';
        endif;
        ?>
        <?= $this->Form->create('Post',
            array(
                'url' => array(
                    'controller' => 'posts',
                    'action' => 'search'
                ),
                array('class' => 'd-flex')
            )
        ) ?>
        <?= $this->Form->input('keyword',
            array(
                'class' => 'form-control me-2',
                'placeholder' => 'Search',
                'div' => false,
                'label' => false,
                'style'=>'width:235px;'
            )
        ) ?>
        <?= $this->Form->button('Search',
            array(
                'class' => 'btn btn-primary',
                'div' => false,
                'style' => 'margin-top: -65px; margin-left: 240px '
            )
        ) ?>
        <?= $this->Form->end() ?>
        <center style='background-color: #8FDDE7'>
            <?= $this->Html->link('List of Users',
                array(
                    'controller' => 'users',
                    'action' => 'viewUsers'
                ),
                array('style' => 'text-decoration: none')
            ) ?>
        </center>
        <hr>
        <center style='background-color: #8FDDE7'>
            Following
        </center>
        <div class='following-scroll'>
        <?php foreach ($following as $follow): ?>
            <br>
            <?php if ($follow['user_id']['profile_pic'] != null): ?>
                <?= $this->Html->image('profiles/'. $follow['user_id']['profile_pic'],
                    array(
                        'class' => 'img-circle'
                    )
                ) ?>
            <?php else: ?>
                <?= $this->Html->image('profiles/user.png',
                    array(
                        'class' => 'img-circle'
                    )
                ) ?>
            <?php endif; ?>
            <?= h($follow['user_id']['username']) ?>
            <?= $this->Html->link('Unfollow',
                array(
                    'controller' => 'followers',
                    'action' => 'unfollow',
                    $follow['user_id']['id']
                ),
                array('style' => 'text-decoration: none')
            ) ?>
        <?php endforeach; ?>
        </div>
        <center>
        <?= $this->Html->link('View All',
            array(
                'controller' => 'followers',
                'action' => 'viewFollowing'
            ),
            array('class' => 'btn btn-primary btn-sm')
        ) ?>
        </center>
        <hr>
        <center style='background-color: #8FDDE7'>
            Followers
        </center>
        <div class='follower-scroll'>
        <?php foreach ($followers as $follower): ?>
        <br>
            <?php if ($follower['follower_user_id']['profile_pic'] != null): ?>
                <?= $this->Html->image('profiles/'. $follower['follower_user_id']['profile_pic'],
                    array(
                        'class' => 'img-circle'
                    )
                ) ?>
            <?php else: ?>
                <?= $this->Html->image('profiles/user.png',
                    array(
                        'class' => 'img-circle'
                    )
                ) ?>
            <?php endif; ?>
            <?= h($follower['follower_user_id']['username']) ?>
            <?= $this->Html->link('Follow',
                array(
                    'controller' => 'followers',
                    'action' => 'follow',
                    $follower['follower_user_id']['id']
                ),
                array('style' => 'text-decoration: none')
            ) ?>
        <?php endforeach; ?>
        </div>
        <center>
        <?= $this->Html->link('View All',
            array(
                'controller' => 'followers',
                'action' => 'viewFollowers'
            ),
            array('class' => 'btn btn-primary btn-sm')
        ) ?>
        </center>
    </div>
</div>