<?= $this->Html->css('home.css') ?>
<div class='col-md-6'>
    <div class='card2'>
        <h3 align='center'>BLOG POSTS</h3>
        <hr>
        <?php foreach ($posts as $post): ?>
            <?php if ($post['user_id']['profile_pic'] != null): ?>
                <?= $this->Html->image('profiles/'. $post['user_id']['profile_pic'],
                    array(
                        'class' => 'img-circle'
                    )
                ) ?>
            <?php else: ?>
                <?= $this->Html->image('profiles/user.png',
                    array(
                        'class' => 'img-circle'
                    )
                ) ?>
            <?php endif; ?>
            <?= $this->Html->link($post['user_id']['username'],
                array(
                    'controller' => 'users',
                    'action' => 'view',
                    $post['user_id']['username']
                ),
                array('style' => 'text-decoration: none')
            ) ?>
            <br>
            <?= nl2br(h($post['Post']['post'])) ?>
                <br>
            <?php if ($post['Post']['image'] != null): ?>
                <?php if (file_exists(WWW_ROOT . 'img/postImages/' . $post['Post']['image'])): ?>
                    <?= $this->Html->image('postImages/'. $post['Post']['image'],
                        array('class' => 'img-post')
                    ) ?>
                <?php else: ?>
                    <?= $this->Html->image('postImages/unavailable-image.jpg',
                        array('class' => 'img-post')
                    ) ?>
                <?php endif; ?>
            <?php endif; ?>
            <br>
            <small><?= h($post['Post']['created']) ?></small>
            <br>
            <center>
                <?= $this->Html->link('Comments',
                    array(
                        'controller' => 'comments',
                        'action' => 'add',
                        $post['Post']['id']
                    ),
                    array('class' => 'btn btn-outline-secondary')
                ) ?>
                <?php foreach ($retweets as $retweet): ?>
                    <?php if ($retweet['Retweet']['post_id'] == $post['Post']['id'] && $retweet['Retweet']['user_id'] == $id): ?>
                        <?= ' ' . $this->Html->image('retweet.png',
                            array(
                                'height' => '20px',
                                'width' => '20px'
                            )
                        ) ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                <?= ' ' . $this->Html->link('Retweet',
                    array(
                        'controller' => 'retweets',
                        'action' => 'retweet',
                        $post['Post']['id']
                    ),
                    array('class' => 'btn btn-outline-info')
                ) ?>
                <?php foreach ($likes as $like): ?>
                    <?php if ($like['PostLike']['post_id'] == $post['Post']['id'] && $like['PostLike']['user_id'] == $id): ?>
                        <?= ' ' . $this->Html->image('like.png',
                            array(
                                'height' => '20px',
                                'width' => '20px'
                            )
                        ) ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                <?= ' ' . $this->Html->link('Like',
                    array(
                        'controller' => 'postLikes',
                        'action' => 'like',
                        $post['Post']['id']
                    ),
                    array('class' => 'btn btn-outline-primary')
                ) ?>
                <?= ' ' . $this->Html->link('Dislike',
                    array(
                        'controller' => 'postLikes',
                        'action' => 'dislike',
                        $post['Post']['id']
                    ),
                    array('class' => 'btn btn-outline-dark')
                ) ?>
            </center>
            <hr><br>
        <?php endforeach; ?>
        <center>
            <?= $this->Paginator->numbers(array('separator' => '')) ?>
        </center>
    </div>
</div>