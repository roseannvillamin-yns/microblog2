<?= $this->Html->css('home.css') ?>
<div class='col-md-3'>
    <div class='card'>
        <div class='chip'>
            <?php if ($profile != null): ?>
                <?= $this->Html->image('profiles/'. $profile,
                    array('class' => 'profile')
                ) ?>
            <?php else: ?>
                <?= $this->Html->image('profiles/user.png',
                    array('class' => 'profile')
                ) ?>
            <?php endif; ?>
        </div>
        <div class='text' align='center'>
            <h1 class='h3 mb-3 font-weight-normal' align='center'>
                <?php if ($loggedIn): ?>
                    <?= $this->Html->link($name,
                        array(
                            'controller' => 'users',
                            'action' => 'view',
                            $username
                        ),
                        array('style' => 'text-decoration: none')
                    ) ?>
                <?php endif; ?>
            </h1>
            <h4><?= h($username) ?></h4>
            <small><?= h($email) ?></small>
            <br><br>
            <div class='container'>
                <div class='row'>
                    <div class='col-sm'>
                        <h5>Following</h5>
                        <?= h($followingCount) ?>
                    </div>
                    <div class='col-sm'>
                        <h5>Followers</h5>
                        <?= h($followerCount) ?>
                    </div>
                    <div class='col-sm'>
                        <h5>Likes</h5>
                        <?= h($likeCount) ?> 
                    </div>
                </div>
            </div>
            
            <br>
            <?= $this->Html->link('Add Post',
                array(
                    'controller' => 'posts',
                    'action' => 'add'
                ),
                array('class' => 'btn btn-primary btn-sm')
            ) ?>
            <?php if ($id != null): ?>
                <?= ' ' . $this->Html->link('Account Settings',
                    array(
                        'controller' => 'users',
                        'action' => 'edit',
                        $id
                    ),
                    array('class' => 'btn btn-secondary btn-sm')
                ) ?>
            <?php endif; ?>
        </div>
        <br><br>
        <div class='text' align='center'>
            <h5 style='background-color: #8FDDE7'>
                Retweeted Posts
            </h5>
        </div>
        <div class='retweet-scroll'>
            <?php foreach ($retweets as $retweet) : ?>
                <small>Retweeted by <?= $retweet['user_id']['username'] ?></small>
                <br>
                <?php if ($retweet['user_id']['profile_pic'] != null): ?>
                    <?= $this->Html->image('profiles/'. $retweet['user_id']['profile_pic'],
                        array(
                            'class' => 'img-circle-retweet'
                        )
                    ) ?>
                <?php else: ?>
                    <?= $this->Html->image('profiles/user.png',
                        array(
                            'class' => 'img-circle-retweet'
                        )
                    ) ?>
                <?php endif; ?>
                <br>
                <?= $retweet['post_id']['post'] ?>
                <br>
                <center>
                <?php if ($retweet['post_id']['image'] != null): ?>
                    <?= $this->Html->image('postImages/'. $retweet['post_id']['image'],
                        array(
                            'height' => '120px',
                            'width' => '150px'
                        )
                    ) ?>
                <?php endif; ?>
                </center>
                <br>
                <small>
                    Retweeted date: <?= $retweet['post_id']['created'] ?>
                    <?php if ($id == $retweet['Retweet']['user_id']): ?>
                        <?= $this->Html->link($this->Html->image('delete.png',
                            array(
                                'height' => '20px',
                                'width' => '20px'
                            )
                        ),
                        array(
                            'controller' => 'retweets',
                            'action' => 'delete',
                            $retweet['Retweet']['id'],
                        ),
                        array(
                            'escape' => false,
                            'confirm' => 'Are you sure you want to delete?'
                        )) ?>
                    <?php endif; ?>
                </small>
                <hr>
            <?php endforeach; ?>
        </div>
    </div>
</div>