<nav class='navbar navbar-dark fixed-top bg-primary'>
	<div class='container'>
		<a class='navbar-brand' href='#'>MICROBLOG 2</a>
		<?= $this->Html->link('LOGIN',
		    array(
		        'controller' => 'users',
		        'action' => 'login',
		    ),
		    array(
		        'class' => 'btn btn-light',
		        'rule' => 'button'
		    )
		) ?>
	</div>
</nav>