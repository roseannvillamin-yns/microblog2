<?= $this->Html->script('bootstrap.min4.js') ?>
<?= $this->Html->css('style.css') ?>
<nav class="navbar navbar-dark fixed-top bg-primary">
	<div class="container">
		<a class="navbar-brand" href="#">MICROBLOG 2</a>
		<div class="btn-group">
		    <?php if ($id != null): ?>
			<?= $this->Html->link('HOME',
			    array(
			    	'controller' => 'posts',
			    	'action' => 'index',
			    ),
			    array(
			    	'class' => 'nav-link',
			    	'style' => 'color: white'
			    )
			) ?>
			<?php else: ?>
			<?= $this->Html->link('LOGIN',
			    array(
			        'controller' => 'users',
			        'action' => 'login'
			    ),
			    array(
			        'class' => 'btn btn-light',
			        'rule' => 'button'
			    )
			) ?>
			<?php endif; ?>
		</div>
	</div>
</nav>