<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
                <div class='card2'>
                    <h3 align='center'>Search Results</h3>
                    <hr>
                    <?php foreach ($results as $result): ?>
                        <?php if ($result['user_id']['profile_pic'] != null): ?>
                            <?= $this->Html->image('profiles/'. $result['user_id']['profile_pic'],
                                array(
                                    'class' => 'img-circle'
                                )
                            ) ?>
                        <?php else: ?>
                            <?= $this->Html->image('profiles/user.png',
                                array(
                                    'class' => 'img-circle'
                                )
                            ) ?>
                        <?php endif; ?>
                        <?= $this->Html->link($result['user_id']['full_name'],
                            array(
                                'controller' => 'users',
                                'action' => 'view',
                                $result['user_id']['username']
                            ),
                            array('style' => 'text-decoration: none')
                        ) ?>
                        <br>
                        <?= h($result['user_id']['username']) ?>
                        <br>
                        <?= nl2br(h($result['Post']['post'])) ?>
                        <br><br>
                        <center>
                            <?= $this->Html->link('Comments',
                                array(
                                    'controller' => 'posts',
                                    'action' => 'view',
                                    $result['Post']['id']
                                ),
                                array('class' => 'btn btn-outline-secondary')
                            ) ?>
                            <?= ' ' . $this->Html->link('Retweet',
                                array(
                                    'controller' => 'retweets',
                                    'action' => 'retweet',
                                    $result['Post']['id']
                                ),
                                array('class' => 'btn btn-outline-info')
                            ) ?>
                            <?php foreach ($likes as $like): ?>
                                <?php if ($like['PostLike']['post_id'] == $result['Post']['id'] && $like['PostLike']['user_id'] == $id): ?>
                                    <?= ' ' . $this->Html->image('like.png',
                                        array(
                                            'height' => '20px',
                                            'width' => '20px'
                                        )
                                    ) ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?= ' ' . $this->Html->link('Like',
                                array(
                                    'controller' => 'postLikes',
                                    'action' => 'like',
                                    $result['Post']['id']
                                ),
                                array('class' => 'btn btn-outline-primary')
                            ) ?>
                            <?= ' ' . $this->Html->link('Dislike',
                                array(
                                    'controller' => 'postLikes',
                                    'action' => 'dislike',
                                    $result['Post']['id']
                                ),
                                array('class' => 'btn btn-outline-dark')
                            ) ?>
                            <?php if ($id == $result['Post']['user_id']): ?>
                                <?= ' ' . $this->Html->image('edit.png',
                                    array(
                                        'height' => '20px',
                                        'width' => '20px',
                                        'style' => 'margin-left: 210px',
                                        'url' => array(
                                            'controller' => 'posts',
                                            'action' => 'edit',
                                            $result['Post']['id']
                                        )
                                    )
                                ) ?>
                                <?= $this->Html->link($this->Html->image('delete.png',
                                    array(
                                        'height' => '20px',
                                        'width' => '20px'
                                    )
                                ), array(
                                    'controller' => 'posts',
                                    'action' => 'delete',
                                    $result['Post']['id']
                                ),
                                array(
                                    'escape' => false,
                                    'confirm' => 'Are you sure you want to delete?'
                                )) ?>
                            <?php endif; ?>
                        </center>
                        <hr>
                    <?php endforeach; ?>
                    <center>
                        <?= $this->Paginator->numbers(array('separator' => '')) ?>
                    </center>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>