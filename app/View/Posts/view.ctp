<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
            	<div class='card2'>
            		<h3 align='center'>Blog Post</h3>
            		<hr>
                    <?php if ($post['user_id']['profile_pic'] != null): ?>
                        <?= $this->Html->image('profiles/'. $post['user_id']['profile_pic'],
                            array(
                                'class' => 'img-circle'
                            )
                        ) ?>
                    <?php else: ?>
                        <?= $this->Html->image('profiles/user.png',
                            array(
                                'class' => 'img-circle'
                            )
                        ) ?>
                    <?php endif; ?>
            		<?= h($post['user_id']['username']) ?>
            		<h4><?= h($post['Post']['post']) ?></h4>
            		<p>
                    <?php if ($post['Post']['image']): ?>
                        <?= $this->Html->image('postImages/'. $post['Post']['image'],
                            array('class' => 'img-post')
                        ) ?>
                    <?php endif; ?>
                    </p>
            		<small>
                        <?= h($post['Post']['created']) ?>
                        <br>
                        <?= h($commentCount) . ' Comments' ?>
            		</small>
            		<br>
            		<?php foreach ($postComments as $postComment): ?>
                        <?php if ($postComment['user_id']['profile_pic'] != null): ?>
                            <?= $this->Html->image('profiles/'. $postComment['user_id']['profile_pic'],
                                array(
                                    'class' => 'img-circle-comment'
                                )
                            ) ?>
                        <?php else: ?>
                            <?= $this->Html->image('profiles/user.png',
                                array(
                                    'class' => 'img-circle-comment'
                                )
                            ) ?>
                        <?php endif; ?>
            			<?= $this->Html->link($postComment['user_id']['username'],
                            array(
                                'controller' => 'users',
                                'action' => 'view',
                                $postComment['user_id']['username']
                            ),
                            array('style' => 'text-decoration: none')
                        ) ?>
            			<?= nl2br(h($postComment['Comment']['comment'])) ?>
            			<?php if ($id == $postComment['Comment']['user_id']): ?>
                            <?= $this->Html->image('edit.png',
                                array(
                                    'height' => '20px',
                                    'width' => '20px',
                                    'url' => array(
                                        'controller' => 'comments', 
                                        'action' => 'edit',
                                        $postComment['Comment']['id']
                                    )
                                )
                            ) ?>
                            <?= $this->Html->link($this->Html->image('delete.png',
                                array(
                                    'height' => '20px',
                                    'width' => '20px'
                                )
                            ), array(
                                'controller' => 'comments',
                                'action' => 'delete',
                                $postComment['Comment']['id']
                            ),
                            array(
                                'escape' => false,
                                'confirm' => 'Are you sure you want to delete?'
                            )) ?>
                        <?php endif; ?>
                        <br><br>
                    <?php endforeach; ?>
                    <?= $this->Paginator->numbers(array('separator' => '')) ?>
                    <?= $this->Form->create('Comment',
                        array(
                    		'url' => array(
                    			'controller' => 'comments',
                    			'action' => 'add',
                    			$post['Post']['id']
                    		)
                    	)
                    ) ?>
                    <?= $this->Form->input('comment',
                        array(
                            'class' => 'form-control',
                            'div' => array(
                                'class' => 'form-label-group'
                            )
                        )
                    ) ?>
                    <br>
                    <?= $this->Form->button('Add Comment',
                            array('class' => 'btn btn-primary')
                    ) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>