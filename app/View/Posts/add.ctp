<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
            	<div class='card2'>
            		<h3 align='center'>Add Post</h3>
            		<hr>
            		<?= $this->Form->create('Post',
                        array(
                            'url' => array(
                                'controller' => 'posts',
                                'action' => 'add'
                            ),
                            'type' => 'file',
                            'class' => 'form-signin'
                        )
                    ) ?>
                    <?= $this->Form->input('post',
                        array(
                            'class' => 'form-control',
                            'div' => array(
                                'class' => $this->Form->isFieldError(
                                    'Post.post') ? 'alert alert-warning' : 'form-label-group'
                            )
                        )
                    ) ?>
                    <?= $this->Form->input('image',
                        array(
                            'type' => 'file',
                            'class' => 'form-control',
                            'div' => array(
                                'class' => $this->Form->isFieldError(
                                    'Post.image') ? 'alert alert-warning' : 'form-label-group'
                            )
                         )
                    ) ?>
                    <br>
                    <?= $this->Form->button('ADD POST',
            			 array('class' => 'btn btn-primary')
            		) ?>
                    <?= $this->Form->end() ?>
            	</div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>



