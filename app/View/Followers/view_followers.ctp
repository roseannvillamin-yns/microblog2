<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
            	<div class='card2'>
            		<h3 align='center'>Followers</h3>
            		<hr>
            		
            		<?php foreach ($userFollowers as $follower): ?>
                        <?php if ($follower['follower_user_id']['profile_pic'] != null): ?>
                            <?= $this->Html->image('profiles/'. $follower['follower_user_id']['profile_pic'],
                                array(
                                    'class' => 'img-circle'
                                )
                            ) ?>
                        <?php else: ?>
                            <?= $this->Html->image('profiles/user.png',
                                array(
                                    'class' => 'img-circle'
                                )
                            ) ?>
                        <?php endif; ?>
            			<?= h($follower['follower_user_id']['full_name']) ?>
                        <br>
            			<?= $this->Html->link($follower['follower_user_id']['username'],
            				array(
                                'controller' => 'users',
                                'action' => 'view',
                                $follower['follower_user_id']['username']
                            ),
                            array('style' => 'text-decoration: none')
                        ) ?>
                        <br><br>
            		<?php endforeach;?>
            		<center><?= $this->Paginator->numbers(array('separator' => '')) ?></center>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>