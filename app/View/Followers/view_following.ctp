<html>
<head>
<title></title>
</head>
<body>
<section id='home' class='container'>
    <div class='col-md-12'>
        <div class='row'>
            <?= $this->element('profile') ?>
            <div class='col-md-6'>
            	<div class='card2'>
            		<h3 align='center'>Following</h3>
            		<hr>
            		
            		<?php foreach ($userFollowing as $follow): ?>
                        <?php if ($follow['user_id']['profile_pic'] != null): ?>
                            <?= $this->Html->image('profiles/'. $follow['user_id']['profile_pic'],
                                array(
                                    'class' => 'img-circle'
                                )
                            ) ?>
                        <?php else: ?>
                            <?= $this->Html->image('profiles/user.png',
                                array(
                                    'class' => 'img-circle'
                                )
                            ) ?>
                        <?php endif; ?>
            			<?= h($follow['user_id']['full_name']) ?>
                        <br>
            			<?= $this->Html->link($follow['user_id']['username'],
            				array(
                                'controller' => 'users',
                                'action' => 'view',
                                $follow['user_id']['username']
                            ),
                            array('style' => 'text-decoration: none')
                        ) ?>
                        <br><br>
            		<?php endforeach; ?>
            		<?= $this->Paginator->numbers(array('separator' => '')) ?>
                </div>
            </div>
            <?= $this->element('follow') ?>
        </div>
    </div>
</section>

</body>
</html>